@isTest
public class AccountOperationsTest {
    @isTest static void testAccountHasDefaultDescription() {       
        // Perform test
        Account acc = new Account(Name='Test Account');
        acc = AccountOperations.setDefaultDescription(acc);
        
        // Verify
        System.assertEquals('Some Updated description', acc.Description);
    }
}